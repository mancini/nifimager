cmake_minimum_required(VERSION 3.20)

project(
  nifimager
  DESCRIPTION "NearFieldImager for LOFAR"
  VERSION 0.0.1
  HOMEPAGE_URL
    https://git.astron.nl/rd/nifimager
  LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")

# Use CMAKE_INSTALL_PREFIX when searching for libraries to run the installed
# vector_add_example program.
include(GNUInstallDirs)
list(APPEND CMAKE_INSTALL_RPATH ${CMAKE_INSTALL_FULL_LIBDIR})


# Download cudawrappers from GitHub
include(FetchContent)

find_package(CUDAToolkit REQUIRED)
find_package(Boost COMPONENTS program_options REQUIRED)



FetchContent_Declare(
  cudawrappers
  GIT_REPOSITORY https://github.com/nlesc-recruit/cudawrappers
  GIT_TAG main)

FetchContent_Declare(
  aocommon
  GIT_REPOSITORY https://gitlab.com/aroffringa/aocommon.git
  GIT_TAG master)
  
FetchContent_MakeAvailable(cudawrappers aocommon)

list(APPEND CMAKE_MODULE_PATH "${aocommon_SOURCE_DIR}/CMake")

find_package(Casacore COMPONENTS casa tables ms)

add_executable(nifimager src/nifimager.cpp)
target_embed_source(nifimager src/kernels/nif_imager.cu)


target_include_directories(nifimager PRIVATE ${CASACORE_INCLUDE_DIRS})
target_link_libraries(nifimager PRIVATE
 Boost::program_options
 cudawrappers::cu 
 cudawrappers::nvrtc 
 ${CASACORE_LIBRARIES}
 xtensor)

install(TARGETS nifimager)