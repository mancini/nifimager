# NifImager
## Near field imager for AARTFAAC measurement sets

The NifImager is an implementation of a near field imager that takes 
heavy inspireation from [LOFAR imaging](https://github.com/lofar-astron/lofarimaging.git).
It allows to produce images of the near field with a specific radius from the 
aperture interferometer using an AARTFAAC measurement set.

The current impleentation has a lot of caveats but it is a first step
towards something more reliable and configurable.

The NifImager only works on Cuda GPU (sorry...).

## Compile
To compile the program we make use of cmake. Therefore, from the source directory you can build it by issuing the following commands:
```
cmake -B build . 
make -C build
```
or if you want to use `Ninja`
```
cmake -B build .
ninja -C build
```

## Usage
To produce `n` slices into a npy container `nifimages.npy` you can issue the following command
```
./nifimager --input_ms [path to the ms] --output nifimages.npy --n_radii [n]
```

You can also adjust the resultion of the image by setting the option `--n_pixels`.
The increasing radii are chosen logaritmically spaced from $10^3$ to $10^8$.
