# coding: utf-8
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from argparse import ArgumentParser


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("input")
    parser.add_argument("output")
    return parser.parse_args()


def main():
    args = parse_args()
    data = np.load(args.input)

    # Create the figure and axes objects
    fig, ax = plt.subplots(
        nrows=max(data.shape[0], 1), ncols=1, sharex=True, sharey=True, figsize=(5, data.shape[0] * 3))
    radii = np.logspace(3.0, 8.0, data.shape[1])

    i = 0
    # Set the initial image
    ims = []
    for t in range(data.shape[0]):
        im = ax[t].imshow(data[t, i, :, :], animated=True)
        ims.append(im)

        cb = plt.colorbar(im, ax=ax[t])
        ax[t].title.set_text(
            f"Image {i} done at radius {radii[i]:.2f} m and time {t}")

    def update(i):
        for t in range(data.shape[0]):
            print(len(ax), len(ims), i, t)
            ax[t].title.set_text(
                f"Image {i} done at radius {radii[i]:.2f} m and time {t}")
            ims[t].set_array(data[t, i, :, :])

        return ims

    # Create the animation object
    animation_fig = animation.FuncAnimation(
        fig, update, frames=data.shape[1], interval=200, blit=True, repeat_delay=10,)
    animation_fig.save(args.output)


if __name__ == '__main__':
    main()
