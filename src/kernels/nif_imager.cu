#define PI_F 3.141592654f
#define SPEED_OF_LIGHT 299792458.0f
#define SQ(A) (A) * (A)
#define D 3

extern "C" __global__ void grid_init(size_t nx, size_t ny, float *grid) {
  short mBx =
      (nx / (gridDim.x * blockDim.x)) + (nx % (gridDim.x * blockDim.x) ? 1 : 0);
  short mBy =
      (ny / (gridDim.y * blockDim.y)) + (ny % (gridDim.y * blockDim.y) ? 1 : 0);
  for (short i = 0; i < mBx; i++) {
    for (short j = 0; j < mBy; j++) {
      int xi =
          (i * gridDim.x * blockDim.x) + blockIdx.x * blockDim.x + threadIdx.x;
      int yi =
          (j * gridDim.y * blockDim.y) + blockIdx.y * blockDim.y + threadIdx.y;

      const float scale = 1.f/float(nx);
      float l = 2.f * ((xi + 0.5f) * scale) - 1.f;
      float m = 2.f * ((yi + 0.5f) * scale) - 1.f;
      float n = sqrtf(1.f - SQ(l) - SQ(m)) - 1.f;
      grid[0 * (nx * ny) + yi * xi] = l;
      grid[1 * (nx * ny) + yi * xi] = m;
      grid[2 * (nx * ny) + yi * xi] = n;
    }
  }
}

#define DIMS 256

extern "C" __global__ void nif_imager(float *antenna_position_a,
                                      float *antenna_position_b, float *radii,
                                      float frequency, float *phases,
                                      float *amplitudes, float *out,
                                      size_t start_radius, size_t nradii,
                                      size_t nvis, size_t nx, size_t ny) {
  int mBx =
      (nx / (gridDim.x * blockDim.x)) + (nx % (gridDim.x * blockDim.x) ? 1 : 0);
  int mBy =
      (ny / (gridDim.y * blockDim.y)) + (ny % (gridDim.y * blockDim.y) ? 1 : 0);

  const float a = 2.f * PI_F * frequency / SPEED_OF_LIGHT;
  const float norm = 1.f / nvis;

  const int nr_threads = blockDim.x * blockDim.y;
  static __shared__ float4 s_antenna_p[2][DIMS];
  static __shared__ float2 s_vis[DIMS];

  float *s_antenna_p_a_ptr = &s_antenna_p[0][0].x;
  float *s_antenna_p_b_ptr = &s_antenna_p[1][0].x;

  int thread_id = threadIdx.y * blockDim.x + threadIdx.x;
  int nbulks = nvis / nr_threads + (nvis % nr_threads ? 1 : 0);
  for (int bulk_id = 0; bulk_id < nbulks; bulk_id += 1) {
    int idx = thread_id + bulk_id * nr_threads;
    if (idx < nvis) {
      for (int l = 0; l < 3; l++) {
        int local_idx = l * nr_threads + thread_id;
        int pos_idx = l * nr_threads + thread_id + bulk_id * nr_threads * 3;

        if (pos_idx < nvis * 3) {
          s_antenna_p_a_ptr[(local_idx * 4) / 3] = antenna_position_a[pos_idx];
          s_antenna_p_b_ptr[(local_idx * 4) / 3] = antenna_position_b[pos_idx];
        }
      }
      s_vis[thread_id].x = amplitudes[idx];
      s_vis[thread_id].y = phases[idx];
    }
    __syncthreads();

    short radius_id = start_radius + blockIdx.z;
    const float radius = radii[radius_id];

    if (radius_id < nradii) {
      float *current_frame = out + (radius_id * nx * ny);
      for (int i = 0; i < mBx; i++) {
        for (int j = 0; j < mBy; j++) {
          const int xi = (i * gridDim.x * blockDim.x) +
                         (blockIdx.x * blockDim.x) + threadIdx.x;
          const int yi = (j * gridDim.y * blockDim.y) +
                         (blockIdx.y * blockDim.y) + threadIdx.y;
          float sum = 0.f;
          if ((xi < nx) && (yi < ny)) {
            const float scale = 1.f/float(nx);
            const float l = 2.f * ((xi + 0.5f) * scale) - 1.f;
            const float m = 2.f * ((yi + 0.5f) * scale) - 1.f;
            const float n = sqrtf(1.f - SQ(l) - SQ(m)) - 1.f;
            const float x = l * radius;
            const float y = m * radius;
            const float z = n * radius;

            if (idx == thread_id) {
              current_frame[yi * nx + xi] = 0.f;
            }
            for (unsigned int s = 0; s < nr_threads; s++) {
              const float d_phase = a * (sqrtf(SQ(s_antenna_p[0][s].x - x) +
                                               SQ(s_antenna_p[0][s].y - y) +
                                               SQ(s_antenna_p[0][s].z - z)) -
                                         sqrtf(SQ(s_antenna_p[1][s].x - x) +
                                               SQ(s_antenna_p[1][s].y - y) +
                                               SQ(s_antenna_p[1][s].z - z)));
              sum += s_vis[s].x * __cosf(d_phase + s_vis[s].y);
            }
          }

          __syncthreads();
          if ((xi < nx) && (yi < ny)) current_frame[yi * nx + xi] += sum * norm;
        }
      }
    }
  }
}