#include <casacore/ms/MeasurementSets.h>
#include <casacore/tables/Tables/ArrayColumn.h>
#include <casacore/tables/Tables/ScalarColumn.h>
#include <casacore/tables/Tables/Table.h>

#include <boost/program_options.hpp>
#include <complex>
#include <cudawrappers/cu.hpp>
#include <cudawrappers/nvrtc.hpp>
#include <iostream>
#include <memory>
#include <xtensor/xadapt.hpp>
#include <xtensor/xarray.hpp>
#include <xtensor/xcomplex.hpp>
#include <xtensor/xfunctor_view.hpp>
#include <xtensor/xindex_view.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xnpy.hpp>
#include <xtensor/xview.hpp>

typedef xt::xarray<float> positions_type;
typedef xt::xarray<float> image_type;
typedef xt::xarray<float> frequency_type;
typedef xt::xarray<std::complex<float>> visibility_type;
using namespace casacore;

void read_visibilities_slice(std::string ms, visibility_type &visibilities,
                             uint slice_id, uint n_correlations) {
  Table tab(ms);
  ArrayColumn<Complex> dataCol(tab, "DATA");
  ArrayColumn<Bool> flagsCol(tab, "FLAG");
  auto shape = dataCol.shape(0);
  uint nchannels = shape[1];
  uint npol = shape[0];
  xt::xarray<bool> flags;

  visibilities.resize({n_correlations, nchannels, npol});
  flags.resize({n_correlations, nchannels, npol});
  uint skip = n_correlations * slice_id * dataCol.get(0).size();

  memcpy(visibilities.data(), dataCol.getColumn().data() + skip,
         sizeof(Complex) * dataCol.get(0).size() * n_correlations);

  memcpy(flags.data(), flagsCol.getColumn().data() + skip,
         sizeof(bool) * flagsCol.get(0).size() * n_correlations);

  xt::filtration(visibilities, flags) = std::complex<float>{0, 0};
}

uint times(std::string ms, uint n_correlations) {
  Table tab(ms);
  ArrayColumn<Complex> dataCol(tab, "DATA");
  uint nrows = dataCol.nrow();
  return nrows / n_correlations;
}

void read_frequencies(MeasurementSet &ms, frequency_type &frequencies) {
  Table tab(ms.spectralWindowTableName());
  ArrayColumn<Double> frequencyCol(tab, "CHAN_FREQ");
  Array<Double> channel_frequencies = frequencyCol.get(0);
  auto shape = frequencyCol.shape(0);
  uint nfreqs = shape[0];
  frequencies.resize({nfreqs});
  xt::xarray<double> tmp_frequencies;
  tmp_frequencies.resize({nfreqs});
  memcpy(tmp_frequencies.data(), channel_frequencies.data(),
         sizeof(double) * nfreqs);
  frequencies = xt::cast<float>(tmp_frequencies);
}

void read_positions(MeasurementSet &ms, positions_type &positions) {
  Table tab(ms.antennaTableName());
  ArrayColumn<Double> positionCol(tab, "POSITION");
  uint nantennas = positionCol.nrow();
  xt::xarray<double> tmp_positions;

  positions.resize({nantennas, 3});
  tmp_positions.resize({nantennas, 3});

  memcpy(tmp_positions.data(), positionCol.getColumn().data(),
         sizeof(double) * nantennas * 3);
  tmp_positions -= xt::mean(tmp_positions, 0);
  positions = xt::cast<float>(tmp_positions);
}

inline uint n_correlation_for_antennas(const uint n_antennas) {
  return static_cast<size_t>(n_antennas * (n_antennas + 1) / 2);
}

void preorder_positions(positions_type &positions, positions_type &antenna1,
                        positions_type &antenna2) {
  const size_t n_antennas = positions.shape(0);
  const size_t n_correlations = n_correlation_for_antennas(n_antennas);
  antenna1.resize({n_correlations, 3});
  antenna2.resize({n_correlations, 3});
  uint count = 0;
  for (uint a = 0; a < n_antennas; a++) {
    for (uint b = 0; b < a + 1; b++) {
      xt::view(antenna1, count, xt::all()) = xt::view(positions, a, xt::all());
      xt::view(antenna2, count, xt::all()) = xt::view(positions, b, xt::all());
      count++;
    }
  }
}

struct cudahandles {
  std::unique_ptr<cu::Device> device;
  std::unique_ptr<cu::Context> context;

  std::unique_ptr<cu::DeviceMemory> d_antenna_positions;
  std::unique_ptr<cu::DeviceMemory> d_antenna_positions_a;
  std::unique_ptr<cu::DeviceMemory> d_antenna_positions_b;

  std::unique_ptr<cu::DeviceMemory> d_image;
  std::unique_ptr<cu::DeviceMemory> d_image_grid;
  std::unique_ptr<cu::DeviceMemory> d_phases;
  std::unique_ptr<cu::DeviceMemory> d_amplitudes;
  std::unique_ptr<cu::DeviceMemory> d_radii;

  std::unique_ptr<cu::HostMemory> h_phases;
  std::unique_ptr<cu::HostMemory> h_amplitudes;
  std::unique_ptr<cu::HostMemory> h_image;

  std::unique_ptr<cu::Stream> stream;
  std::unique_ptr<nvrtc::Program> program;
  std::unique_ptr<cu::Module> module;
  std::unique_ptr<cu::Function> nif_imager;
  std::unique_ptr<cu::Function> grid_init;
};

void initialize_kernel(struct cudahandles &handles) {
  handles.device = std::make_unique<cu::Device>(0);
  handles.context = std::make_unique<cu::Context>(CU_CTX_SCHED_BLOCKING_SYNC,
                                                  *handles.device);

  extern const char _binary_src_kernels_nif_imager_cu_start,
      _binary_src_kernels_nif_imager_cu_end;

  const std::string kernel(&_binary_src_kernels_nif_imager_cu_start,
                           &_binary_src_kernels_nif_imager_cu_end);
  handles.program = std::make_unique<nvrtc::Program>(kernel, "nif_imager.cu");

  // compile kernel
  std::vector<std::string> options = {"-use_fast_math", "-lineinfo"};

  try {
    handles.program->compile(options);
  } catch (nvrtc::Error &error) {
    std::cerr << handles.program->getLog();
    throw;
  }

  handles.module = std::make_unique<cu::Module>(
      static_cast<const void *>(handles.program->getPTX().data()));
  handles.nif_imager =
      std::make_unique<cu::Function>(*handles.module, "nif_imager");
  handles.grid_init =
      std::make_unique<cu::Function>(*handles.module, "grid_init");
}

void zero_autocorrelations(visibility_type &vis, size_t n_antennas) {
  uint current = 0;
  for (uint i = 0; i < n_antennas; i++) {
    vis[current + i] = std::complex<float>(0.f, 0.f);
    current += i;
  }
}

void allocate_device_memory(positions_type &antenna_positions_a,
                            positions_type &antenna_positions_b,
                            uint n_correlations, uint n_pixels, uint n_radii,
                            struct cudahandles &device_handles) {
  device_handles.stream = std::make_unique<cu::Stream>();

  device_handles.d_antenna_positions_a =
      std::make_unique<cu::DeviceMemory>(3 * n_correlations * sizeof(float));
  device_handles.d_antenna_positions_b =
      std::make_unique<cu::DeviceMemory>(3 * n_correlations * sizeof(float));

  device_handles.stream->memcpyHtoDAsync(*device_handles.d_antenna_positions_a,
                                         antenna_positions_a.data(),
                                         3 * n_correlations * sizeof(float));

  device_handles.stream->memcpyHtoDAsync(*device_handles.d_antenna_positions_b,
                                         antenna_positions_b.data(),
                                         3 * n_correlations * sizeof(float));

  device_handles.d_image_grid =
      std::make_unique<cu::DeviceMemory>(n_pixels * n_pixels * sizeof(float));

  device_handles.d_image = std::make_unique<cu::DeviceMemory>(
      n_radii * n_pixels * n_pixels * sizeof(float));
  device_handles.d_phases =
      std::make_unique<cu::DeviceMemory>(n_correlations * sizeof(float));
  device_handles.d_amplitudes =
      std::make_unique<cu::DeviceMemory>(n_correlations * sizeof(float));
  device_handles.d_radii =
      std::make_unique<cu::DeviceMemory>(n_radii * sizeof(float));

  device_handles.h_phases =
      std::make_unique<cu::HostMemory>(n_correlations * sizeof(float));
  device_handles.h_amplitudes =
      std::make_unique<cu::HostMemory>(n_correlations * sizeof(float));
  device_handles.h_image = std::make_unique<cu::HostMemory>(
      n_radii * n_pixels * n_pixels * sizeof(float));
}

void process_visibility_slice(visibility_type &visibilities, float frequency,
                              xt::xarray<float> &radii, size_t nx, size_t ny,
                              struct cudahandles &handles) {
  xt::adapt((float *)*handles.h_phases, visibilities.shape()) =
      xt::angle(visibilities);
  xt::adapt((float *)*handles.h_amplitudes, visibilities.shape()) =
      xt::sqrt(xt::norm(visibilities));
  int radius_id = 0;
  size_t nradii = radii.size();
  size_t n_correlations = visibilities.shape(0);

  handles.stream->memcpyHtoDAsync(*handles.d_phases, *handles.h_phases,
                                  sizeof(float) * visibilities.size());
  handles.stream->memcpyHtoDAsync(*handles.d_amplitudes, *handles.h_amplitudes,
                                  sizeof(float) * visibilities.size());

  handles.stream->memcpyHtoDAsync(*handles.d_radii, radii.data(),
                                  sizeof(float) * nradii);

  std::vector<const void *> parameters = {
      handles.d_antenna_positions_a->parameter(),
      handles.d_antenna_positions_b->parameter(),
      handles.d_radii->parameter(),
      &frequency,
      handles.d_phases->parameter(),
      handles.d_amplitudes->parameter(),
      handles.d_image->parameter(),
      &radius_id,
      &nradii,
      &n_correlations,
      &nx,
      &ny};

  std::cout << "Processing nradii " << nradii << std::endl;
  cu::Event start, stop;

  handles.stream->record(start);

  handles.stream->launchKernel(*handles.nif_imager, 32, 32, nradii, 16, 16, 1,
                               0, parameters);
  handles.stream->record(stop);

  handles.stream->memcpyDtoHAsync(*handles.h_image, *handles.d_image,
                                  nradii * nx * ny * sizeof(float));
  std::cout << "Waiting GPU execution to finish ... please wait..."
            << std::endl;
  handles.stream->synchronize();
  std::cout << "Kernel execution took :" << stop.elapsedTime(start) << " ms"
            << std::endl;
}

struct nif_parameters {
  std::string ms_path;
  std::string outfile;
  uint npixels;
  uint nradii;
} parameters;

int parse_args(struct nif_parameters &parameters, int argc, char *argv[]) {
  namespace po = boost::program_options;
  using namespace std;
  po::options_description desc("Allowed options");
  desc.add_options()("help", "produce help message")(
      "input_ms", po::value<std::string>(), "Input measurement set")(
      "output", po::value<std::string>(), "Output fits")(
      "n_radii", po::value<uint>()->default_value(50),
      "Number of radii to compute")(
      "n_pixels", po::value<uint>()->default_value(1024), "Number of pixels");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    cout << desc << "\n";
    return 1;
  }

  if (vm.count("input_ms") + vm.count("output") != 2) {
    std::cerr << "Invalid number of arguments " << std::endl;
    std::cerr << desc << std::endl;
    return 1;
  } else {
    parameters.ms_path = vm["input_ms"].as<std::string>();
    parameters.outfile = vm["output"].as<std::string>();
    parameters.npixels = vm["n_pixels"].as<uint>();
    parameters.nradii = vm["n_radii"].as<uint>();

    if (parameters.nradii < 1) {
      std::cerr << "Invalid radii number " << parameters.nradii << std::endl;
      return 1;
    }
  }
  return 0;
}

void precompute(struct cudahandles &device_handles, size_t nx, size_t ny) {
  std::vector<const void *> parameters = {
      &nx, &ny, device_handles.d_image_grid->parameter()};
  device_handles.stream->launchKernel(*device_handles.grid_init, 32, 32, 1, 32,
                                      32, 1, 0, parameters);
}

void nifimager(const std::string msin, const std::string out,
               const uint npixels, const uint nradii) {
  struct cudahandles deviceHandles;
  visibility_type data;
  frequency_type frequencies;
  positions_type positions;
  positions_type positions_ant_a;
  positions_type positions_ant_b;

  std::cout << "Reading data" << std::endl;
  MeasurementSet ms(msin);

  image_type image_cube;

  read_positions(ms, positions);

  preorder_positions(positions, positions_ant_a, positions_ant_b);

  const uint n_antennas = positions.shape(0);
  const uint n_correlations = n_correlation_for_antennas(n_antennas);
  read_frequencies(ms, frequencies);
  const uint ntimes = times(msin, n_correlations);
  initialize_kernel(deviceHandles);

  allocate_device_memory(positions_ant_a, positions_ant_b, n_correlations,
                         npixels, nradii, deviceHandles);

  auto t_image_slice =
      xt::adapt((float *)*deviceHandles.h_image, {nradii, npixels, npixels});

  image_cube.resize({ntimes, nradii, npixels, npixels});
  xt::xarray<float> radii = xt::logspace<float>(5.f, 8.f, nradii);
  for (uint t = 0; t < ntimes; t++) {
    read_visibilities_slice(msin, data, t, n_correlations);
    zero_autocorrelations(data, n_antennas);
    std::cout << "Preprocessing time " << t << " with correlations "
              << n_correlations << " and nradii " << nradii << std::endl;

    const int XXPOL = 0;
    const int YYPOL = 3;
    data = xt::view(data, xt::all(), xt::all(), XXPOL) +
           xt::view(data, xt::all(), xt::all(), YYPOL);
    data = xt::nanmean(data, 1);
    float frequency = xt::mean(frequencies, 0)();

    std::cout << "Computing at frequency " << frequency << std::endl;

    process_visibility_slice(data, frequency, radii, npixels, npixels,
                             deviceHandles);

    xt::view(image_cube, t, xt::all(), xt::all(), xt::all()) = t_image_slice;
  }
  std::cout << "Processed" << std::endl;

  xt::dump_npy(out, image_cube);
}

int main(int argc, char *argv[]) {
  if (parse_args(parameters, argc, argv)) {
    return 1;
  }
  std::cout << parameters.ms_path << std::endl;
  cu::init();
  nifimager(parameters.ms_path, parameters.outfile, parameters.npixels,
            parameters.nradii);
}